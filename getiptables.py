#!/usr/bin/python

# date: 4JAN2017
# author:  mleo
# purpose: collect iptables and send to security
# version: 1.0

# import modules/packages for use later:
import subprocess
import sys
import os
import os.path
import socket

# commands
SSH_COMMAND = "/sbin/iptables -n -v -L"

# now do stuff:
# read in hosts.txt file
#hostfile = open("/data/scripts/hosts.txt","r").read().split('\n')
hostfile = open("/data/datadir/master_list","r").read().split('\n')
# LOOP for each host
for host in hostfile:
   try:
     addr = socket.gethostbyname(host)
   except:
     pass
   print(host)
   if (host == "chpass1.unix.fas.harvard.edu") or (host == "kb.unix.fas.harvard.edu") or (host == "linux1.unix.fas.harvard.edu") or (host == "proxy1.unix.fas.harvard.edu") or (host == "proxy2.unix.fas.harvard.edu"):
      continue
   if (addr):
      output_file = open(os.path.join("/data/scripts/getiptables/iptables",addr),"w")
      ssh = subprocess.Popen(["ssh", "%s" % host, SSH_COMMAND],
                             shell=False,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)
      result = ssh.stdout.readlines()
      if result == []:
         error = ssh.stderr.readlines()
         print >>output_file, "ERROR: %s" % error
      else: 
         for line in result:
            print>>output_file, line.rstrip('\n')
      output_file.close()
   else:
      continue

# send only new files to repohost
# for this to work, this host needs ssh/keys configured outside this script
# for each NEW file in each directory in iptables/*, loop
os.system("rsync -e 'ssh -q -i /data/scripts/getiptables/overseer_key' -r `find /data/scripts/getiptables/iptables/* -type f -mtime -1` idsxfer@overseer.soc.harvard.edu:/idsxfer/iptables")
